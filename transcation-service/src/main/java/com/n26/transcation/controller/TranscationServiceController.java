package com.n26.transcation.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/***
 * update the comments
 *
 */
@RestController
@Api(value="Transcation Service Information for Swagger")
@RequestMapping("/transcation")
public class TranscationServiceController {
	
	
	@Autowired
	private TranscationService transcationService;
	
	
	/**
	 * Get All The Transcations for testing purpose
	 * @return
	 */
	@GetMapping("/getAllTranscation")
	@HystrixCommand(fallbackMethod="getAllTranscationFB")
	@ApiOperation(value="Get All The Transcations", response=Transcation.class, responseContainer="List")
	public ResponseEntity<List<Transcation>> getAllTranscations() {
		List<Transcation> listtranscation = transcationService.getAllTranscation();
		return ResponseEntity.ok(listtranscation);
	}
	
	public ResponseEntity<List<Transcation>> getAllTranscationFB() {
		List<Transcation> listEmployees = transcationService.getAllTranscationFB();
		return ResponseEntity.ok(listEmployees);
	}
	
	
	/**
	 * Adding Transcation
	 * @param transcation
	 * @return
	 */
	@PostMapping("/transcations")
	@HystrixCommand(fallbackMethod="addTranscationFB")
	@ApiOperation(value="Add Transcation", response=Transcation.class)
	public ResponseEntity<?> addEmployee(@Valid @RequestBody Transcation transcation) {
		try {
		transcationService.addTranscation(transcation);
		return new ResponseEntity<>(HttpStatus.CREATED);
		}catch(TranscationOverSixtySeconds tot) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	public ResponseEntity<?> addTranscationFB(@Valid @RequestBody Transcation transcation) {
		transcationService.addTranscationFB(transcation);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Get All The Statistics
	 * @return
	 */
	@GetMapping("/statistics")
	@HystrixCommand(fallbackMethod="getStatisticsFB")
	@ApiOperation(value="Get Statistics", response=Statistics.class)
	public ResponseEntity<?> getStatistics() {
		Statistics statistics = transcationService.getStatistics();
		 return new ResponseEntity<>(statistics,HttpStatus.OK);
	}
	
	public ResponseEntity<?> getStatisticsFB() {
		transcationService.getStatisticsFB();
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	
}
