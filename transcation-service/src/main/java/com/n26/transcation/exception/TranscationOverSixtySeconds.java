package com.n26.transcation.exception;

/**
 * If the Epoch time is greater than 60 seconds
 * @return
 */
public class TranscationOverSixtySeconds extends Exception {

}
