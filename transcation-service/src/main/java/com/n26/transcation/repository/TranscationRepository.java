package com.n26.transcation.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.n26.transcation.model.Transcation;


/***
 * Transcation repository
 *
 */
@Repository
@Transactional
public interface TranscationRepository extends JpaRepository<Transcation, Long> {
	
	

}
