package com.n26.transcation.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;


/***
 * TranscationService interface
 *
 */
@Service
public interface TranscationService {
	
	
	/**
	 * Get All the transcations
	 * @return
	 */
	public List<Transcation> getAllTranscation();
	
	

	/**
	 * Add a Transcation
	 * @param transcation
	 * @return
	 */
	public Transcation addTranscation(Transcation transcation)throws TranscationOverSixtySeconds;
	
	
	/**
	 * get statistics for last 60 seconds
	 * @return
	 */
	public Statistics getStatistics();
	
	
	/**
	 * Fallback method for getAllTranscation
	 * @return
	 */
	public List<Transcation> getAllTranscationFB();
	
	
	
	/**
	 * Fallback method for addTranscation
	 * @param transcation
	 * @return
	 */
	public Transcation addTranscationFB(Transcation transcation);
	
	
	/**
	 * Fallback method for getStatistics
	 * @return
	 */
	public Statistics getStatisticsFB();

}
