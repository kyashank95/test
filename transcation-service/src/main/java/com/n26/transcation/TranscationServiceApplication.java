package com.n26.transcation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * update the comments
 *
 */
@SpringBootApplication
//@EnableEurekaClient
//@EnableCircuitBreaker
//@EnableHystrixDashboard
//@EnableFeignClients //delete this annotation if not using feign client
@EnableCaching
//@EnableSqs //delete this annotation if not using AWS SQS
//@RefreshScope
@EnableSwagger2
//@ComponentScan({"com.n26.transcation.*"})
public class TranscationServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(TranscationServiceApplication.class, args);
	}
	
}
