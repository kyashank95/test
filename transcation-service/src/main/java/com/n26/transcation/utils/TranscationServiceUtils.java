package com.n26.transcation.utils;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Transcation;

public class TranscationServiceUtils {
	
	public static void calculateTranscationTime(Transcation transcation) throws TranscationOverSixtySeconds {
		long currTime = currentTimeStamp();
		long enteredTimeStamp = transcation.getTimestamp();
		long gapInTimeStamp = currTime - enteredTimeStamp;
		long gapInTimeStampInMilliSec = gapInTimeStamp/1000;
		
		if(gapInTimeStampInMilliSec > 60) {
			throw new TranscationOverSixtySeconds();
		}
	}
	
	public static long currentTimeStamp() {
		long currTimeStamp = System.currentTimeMillis();
		return currTimeStamp;
	}


}
