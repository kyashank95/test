package com.n26.transcation.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/***
 * update the comments
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	/**
	 * update the comments
	 * @return
	 */
	@Bean
	public Docket sampleApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.n26.transcation.controller")).paths(PathSelectors.any()).build()
				.apiInfo(metaData());
	}

	
	/**
	 * update the comments
	 * @return
	 */
	private ApiInfo metaData() {
		ApiInfo apiMeta = new ApiInfo("Transcation-Service API", "Performs all operations on Transcation resource", "1.0",
				"No terms of service", new Contact("name", "web-site", "email"),
				"No license", "web-site");
		return apiMeta;
	}

	
}