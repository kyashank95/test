package com.n26.transcation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transcation.controller.TranscationServiceController;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TranscationService.class, secure = false)
//@SpringBootTest
public class TranscationServiceApplicationTests {
	
	
	@MockBean
	private TranscationService transcationService;
	
	@Autowired
	private MockMvc mockMvc;
	
	

	@Test
	public void mockAddTranscation() throws Exception {
		
		Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(1579205840000L);
		
		String inputJson = this.mapToJson(mockTranscation);
		
		Mockito.when(transcationService.addTranscation(Mockito.any()))
		.thenReturn(mockTranscation);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/transcation/transcations")
				.accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		//MockHttpServletResponse response = result.getResponse();
		String outputJson = result.getResponse().getContentAsString();
		
		System.out.println("InputJson------->"+inputJson);
		System.out.println("OutputJson------->"+outputJson);
		//assertThat(outputJson).isEqualTo(inputJson);
		JSONAssert.assertEquals(inputJson, outputJson, false);
	}
	
	
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
