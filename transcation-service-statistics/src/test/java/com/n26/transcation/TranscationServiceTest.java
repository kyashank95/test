package com.n26.transcation;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.rules.ExpectedException.none;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;
import com.n26.transcation.service.TranscationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TranscationServiceTest {

    @Rule
    public final ExpectedException exceptionThrown = none();
    
    @MockBean
    private TranscationService transcationService;

    private Map<Long,List<Transcation>> map = new HashMap<>(); 
    
    @Before
    public void setUp() throws Exception {
        transcationService = new TranscationServiceImpl();
        this.map = map;
    }

    @Test
    public void testSaveTheTransaction() throws TranscationOverSixtySeconds {
        Transcation validTranscation = new Transcation(1009, System.currentTimeMillis());

        transcationService.addTranscation(validTranscation);
        assertThat(map.get(System.currentTimeMillis())).isEqualTo(validTranscation);
    }

    @Test
    public void shouldConsiderLast60SecTransactionsInStatistics() throws TranscationOverSixtySeconds {
        
    	List<Transcation> list = new ArrayList<>();
    	double amount = 1009;
        long transcationTime = System.currentTimeMillis();
        long staleTranscationTime = Instant.now(Clock.systemUTC()).minusSeconds(61).toEpochMilli();
        Transcation validTranscation = new Transcation(amount, transcationTime);
        list.add(validTranscation);
        Transcation staleTranscation = new Transcation(amount, staleTranscationTime);
        list.add(staleTranscation);
        map.put(transcationTime, list);
        map.put(transcationTime, list);

        Statistics transcationStatistics = new Statistics(1009, 1009, 1009, 1009,1);

        Statistics currentStatistics = transcationService.getStatistics();
        assertThat(currentStatistics).isEqualTo(transcationStatistics);
    }

    @Test
    public void shouldThrowExceptionIfTransactionIsExpired() throws TranscationOverSixtySeconds {
        
    	Transcation mockTranscation = new Transcation(1009,1579205840000L);
        exceptionThrown.expect(TranscationOverSixtySeconds.class);

       transcationService.addTranscation(mockTranscation);
    }

}
