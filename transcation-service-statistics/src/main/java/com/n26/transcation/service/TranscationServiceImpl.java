package com.n26.transcation.service;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;


/***
 * Transcation Service Implementation 
 *
 */
@Service
public class TranscationServiceImpl implements TranscationService {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	
	private List<Transcation> listTranscation = new ArrayList<>();
	private Map<Date,List<Transcation>> mapTranscation = new HashMap<>();
	
	@Override
	public Map<Date,List<Transcation>> getAllTranscation() {
		return mapTranscation;
	}


	@Override
	public Map<Date,List<Transcation>> addTranscation(Transcation transcation) throws TranscationOverSixtySeconds {
		calculateTranscationTime(transcation);
		if(mapTranscation.containsKey(transcation.getTimestamp())) {
			listTranscation.add(transcation);
			mapTranscation.put(transcation.getTimestamp(), listTranscation);			
		}
		else {
			listTranscation = new ArrayList<>();
			listTranscation.add(transcation);
			mapTranscation.put(transcation.getTimestamp(), listTranscation);
		}
		return mapTranscation;
	}


	@Override
	public Statistics getStatistics() {
		//long currentTimeStamp = currentTimeStamp();
		List<Transcation> transcationList = new ArrayList<>();
		mapTranscation.entrySet().stream().forEach(transcationObject ->{ 			
			Date d = transcationObject.getKey();
			long eachTimeStamp = d.getTime()-19800000;
			long timeCheck = (currentTimeStamp() - eachTimeStamp)/1000;
			if(timeCheck <= 60) {
				transcationObject.getValue().stream().forEach(correctTranscations -> {
					transcationList.add(correctTranscations);
				});
			}
			});
		return calculateStatistics(transcationList);
	}


	private Statistics calculateStatistics(List<Transcation> transcationList) {
		
		 BigDecimal sum[] = {BigDecimal.ZERO};
		 BigDecimal avg[] = {BigDecimal.ZERO};
		 BigDecimal max[] = {BigDecimal.ZERO};
		 BigDecimal min[] = {BigDecimal.ZERO};
		 long count[] = {0L};
	
		if(!transcationList.isEmpty()) {
			min[0] = (transcationList.get(0).getAmount()).setScale(2,RoundingMode.HALF_UP);
		transcationList.stream().forEach(trans -> {
			sumMaxMinCal(sum, max, min, trans);
			});
		countAvgCal(transcationList, sum, avg, count);
		}	
		Statistics responseStatistics = new Statistics(sum[0], avg[0], max[0], min[0], count[0]);
		return responseStatistics;
	}

	private void countAvgCal(List<Transcation> transcationList, BigDecimal[] sum, BigDecimal[] avg, long[] count) {
		count[0] = transcationList.size();
		avg[0] = (sum[0].divide(new BigDecimal(count[0]))).setScale(2,RoundingMode.HALF_UP);
	}

	private void sumMaxMinCal(BigDecimal[] sum, BigDecimal[] max, BigDecimal[] min, Transcation trans) {
		sum[0] = sum[0].add(trans.getAmount()).setScale(2,RoundingMode.HALF_UP);
		if(max[0].compareTo(trans.getAmount()) == -1) {
			max[0] = trans.getAmount().setScale(2,RoundingMode.HALF_UP);
		}
		if(min[0].compareTo(trans.getAmount()) == 1) {
			min[0] = trans.getAmount().setScale(2,RoundingMode.HALF_UP);
		}
	}
	
	private void calculateTranscationTime(Transcation transcation) throws TranscationOverSixtySeconds {	
		
		Date d1=null;
		try {
			d1 = sdf.parse(sdf.format(transcation.getTimestamp()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if((((currentTimeStamp())-((d1.getTime()-19800000)))/1000) > 60) {
			throw new TranscationOverSixtySeconds();
		}
	}
	
	private long currentTimeStamp() {
		return System.currentTimeMillis();
	}


	@Override
	public void deleteAllTransactions() {
		mapTranscation.clear();
		
	}
	
	
}
