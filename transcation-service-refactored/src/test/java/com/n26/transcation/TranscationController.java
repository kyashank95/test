package com.n26.transcation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.n26.transcation.controller.TranscationServiceController;
import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;
import com.sun.istack.internal.NotNull;

@RunWith(MockitoJUnitRunner.class)
public class TranscationController {
	
	@Mock
	private TranscationService transcationService;
	private TranscationServiceController transcationController;
	
	@Before
    public void setUp() throws Exception {
		transcationController = new TranscationServiceController(transcationService);
    }
	
	@Test
	public void testSaveTranscationAndReturnCreatedResponse() throws TranscationOverSixtySeconds {
		
		Transcation mockTranscation = new Transcation(1009,System.currentTimeMillis());
		
		ResponseEntity<?> response = transcationController.addTranscation(mockTranscation);
		verify(transcationService).addTranscation(mockTranscation);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}
	
	@Test
	public void testSaveTranscationAndReturnNoContentResponse() throws TranscationOverSixtySeconds {
		
		Transcation mockTranscation = new Transcation(1009,1579205840000L);
		
		doThrow(new TranscationOverSixtySeconds(mockTranscation))
        .when(transcationService)
        .addTranscation(mockTranscation);
		
		ResponseEntity<?> response = transcationController.addTranscation(mockTranscation);
		verify(transcationService).addTranscation(mockTranscation);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	}
	
	@Test
	public void testShowStatisticsAndReturnOkResponse()  {
		
		ResponseEntity<?> response = transcationController.getStatistics();
		verify(transcationService).getStatistics();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void testShowEnteredRequestBodyIsValid() throws TranscationOverSixtySeconds  {
		
		Transcation mockTranscation = new Transcation(1009,System.currentTimeMillis());
        assertThat(mockTranscation.getAmount()).isNotEqualTo(null);
        assertThat(mockTranscation.getTimestamp()).isNotEqualTo(null);
	}
}
