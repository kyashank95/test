package com.n26.transcation.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;


/***
 * Transcation Service Rest Controller
 *
 */
@RestController
@RequestMapping("/transcation")
public class TranscationServiceController {
	
	
	@Autowired
	private TranscationService transcationService;
	
	
	 @Autowired
	    public TranscationServiceController(TranscationService transcationService) {
	        this.transcationService = transcationService;
	    }
	
	/**
	 * Get All The Transcations for testing purpose
	 * @return
	 */
	@GetMapping("/getAllTranscation")
	public ResponseEntity<Map<Long,List<Transcation>>> getAllTranscations() {
		Map<Long,List<Transcation>> listtranscation = transcationService.getAllTranscation();
		return ResponseEntity.ok(listtranscation);
	}	
	/**
	 * Adding Transcation
	 * @param transcation
	 * @return
	 */
	@PostMapping("/transcations")
	public ResponseEntity<?> addTranscation(@Valid @RequestBody Transcation transcation) {
		try {
		transcationService.addTranscation(transcation);
		return new ResponseEntity<>(HttpStatus.CREATED);
		}catch(TranscationOverSixtySeconds tot) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	/**
	 * Get All The Statistics
	 * @return
	 */
	@GetMapping("/statistics")
	public ResponseEntity<?> getStatistics() {
		Statistics statistics = transcationService.getStatistics();
		 return new ResponseEntity<>(statistics,HttpStatus.OK);
	}	
}
