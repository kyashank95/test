package com.n26.transcation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Transcation Service Spring Boot Application file
 *
 */
@SpringBootApplication
public class TranscationServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(TranscationServiceApplication.class, args);
	}
	
}
