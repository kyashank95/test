package com.n26.transcation.model;

import java.io.Serializable;

import com.sun.istack.internal.NotNull;


public class Transcation implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	private double amount;
	
	@NotNull
	private long timestamp;
	
	
	public Transcation() {
		// TODO Auto-generated constructor stub
	}


	public Transcation(double amount, long timestamp) {
		super();
		this.amount = amount;
		this.timestamp = timestamp;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public long getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	
	

}
