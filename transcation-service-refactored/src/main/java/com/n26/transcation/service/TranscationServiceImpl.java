package com.n26.transcation.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;


/***
 * Transcation Service Implementation 
 *
 */
@Service
public class TranscationServiceImpl implements TranscationService {

	
	
	private List<Transcation> listTranscation = new ArrayList<>();
	private Map<Long,List<Transcation>> mapTranscation = new HashMap<>();
	
	@Override
	public Map<Long,List<Transcation>> getAllTranscation() {
		return mapTranscation;
	}


	@Override
	public Map<Long,List<Transcation>> addTranscation(Transcation transcation) throws TranscationOverSixtySeconds {
		calculateTranscationTime(transcation);
		if(mapTranscation.containsKey(transcation.getTimestamp())) {
			listTranscation.add(transcation);
			mapTranscation.put(transcation.getTimestamp(), listTranscation);			
		}
		else {
			listTranscation = new ArrayList<>();
			listTranscation.add(transcation);
			mapTranscation.put(transcation.getTimestamp(), listTranscation);
		}
		return mapTranscation;
	}


	@Override
	public Statistics getStatistics() {
		long currentTimeStamp = currentTimeStamp();
		List<Transcation> transcationList = new ArrayList<>();
		mapTranscation.entrySet().stream().forEach(transcationObject ->{ 
			long eachTimeStamp = transcationObject.getKey();
			long timeCheck = (currentTimeStamp - eachTimeStamp)/1000;
			if(timeCheck <= 60) {
				transcationObject.getValue().stream().forEach(correctTranscations -> {
					transcationList.add(correctTranscations);
				});
			}
			});
		return calculateStatistics(transcationList);
	}


	private Statistics calculateStatistics(List<Transcation> transcationList) {
		
		 double sum[] = {0.0};
		 double avg[] = {0.0};
		 double max[] = {0.0};
		 double min[] = {0.0};
		 long count[] = {0L};
	
		if(!transcationList.isEmpty()) {
			min[0] = Double.valueOf(transcationList.get(0).getAmount());
		transcationList.stream().forEach(trans -> {
			sumMaxMinCal(sum, max, min, trans);
			});
		countAvgCal(transcationList, sum, avg, count);
		}	
		Statistics responseStatistics = new Statistics(sum[0], avg[0], max[0], min[0], count[0]);
		return responseStatistics;
	}

	private void countAvgCal(List<Transcation> transcationList, double[] sum, double[] avg, long[] count) {
		count[0] = transcationList.size();
		avg[0] = (sum[0] /count[0]);
	}

	private void sumMaxMinCal(double[] sum, double[] max, double[] min, Transcation trans) {
		sum[0] = sum[0] + trans.getAmount();
		if(max[0] <= trans.getAmount()) {
			max[0] = trans.getAmount();
		}
		if(min[0] >= trans.getAmount()) {
			min[0] = trans.getAmount();
		}
	}
	
	private void calculateTranscationTime(Transcation transcation) throws TranscationOverSixtySeconds {			
		if((((currentTimeStamp())-(transcation.getTimestamp()))/1000) > 60) {
			throw new TranscationOverSixtySeconds();
		}
	}
	
	private long currentTimeStamp() {
		return System.currentTimeMillis();
	}
	
}
