package com.n26.transcation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.n26.transcation.controller.TranscationServiceController;
import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.exception.TranscationServiceUnprocessable;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;


@RunWith(MockitoJUnitRunner.class)
public class TranscationController {
	
	@Mock
	private TranscationService transcationService;
	private TranscationServiceController transcationController;
	
	long fiveHrs30Min = 19800000;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	@Before
    public void setUp() throws Exception {
		transcationController = new TranscationServiceController(transcationService);
    }
	
	@Test
	public void testSaveTranscationAndReturnCreatedResponse() throws TranscationOverSixtySeconds,Exception {
		
		Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min)));
		
		ResponseEntity<?> response = transcationController.addTranscation(mockTranscation);
		verify(transcationService).addTranscation(mockTranscation);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}
	
	@Test
	public void testSaveTranscationAndReturnNoContentResponse() throws TranscationOverSixtySeconds,Exception {
		
		Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime()))));
		
		doThrow(new TranscationOverSixtySeconds(mockTranscation))
        .when(transcationService)
        .addTranscation(mockTranscation);
		
		ResponseEntity<?> response = transcationController.addTranscation(mockTranscation);
		verify(transcationService).addTranscation(mockTranscation);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	}
	
	@Test
	public void testSaveTranscationAndReturnUnProcessableResponse() throws TranscationServiceUnprocessable,Exception {
		
		Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime()+fiveHrs30Min+10000))));
		
		doThrow(new TranscationServiceUnprocessable(mockTranscation))
        .when(transcationService)
        .addTranscation(mockTranscation);
		
		ResponseEntity<?> response = transcationController.addTranscation(mockTranscation);
		verify(transcationService).addTranscation(mockTranscation);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	@Test
	public void testShowStatisticsAndReturnOkResponse()  {
		
		ResponseEntity<?> response = transcationController.getStatistics();
		verify(transcationService).getStatistics();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void testShowEnteredRequestBodyIsValid() throws TranscationOverSixtySeconds,Exception  {
		
		Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min)));
        assertThat(mockTranscation.getAmount()).isNotEqualTo(null);
        assertThat(mockTranscation.getTimestamp()).isNotEqualTo(null);
	}
	
	@Test
	public void testDeleteTranscationAndReturnNoContentResponse() throws TranscationOverSixtySeconds,Exception {
		
		ResponseEntity<?> response = transcationController.deleteTransactions();
		verify(transcationService).deleteAllTransactions();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	}
	
}
