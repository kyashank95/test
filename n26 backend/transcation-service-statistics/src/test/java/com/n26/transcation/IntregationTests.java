package com.n26.transcation;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;


@RunWith(SpringRunner.class)
@SpringBootTest(classes =TranscationServiceApplication.class)
@AutoConfigureMockMvc
public class IntregationTests {
    
	@Autowired
    private MockMvc mockMvc;
   
	long fiveHrs30Min = 19800000;
	long thirtySec = 30000;
	long twelveSec = 12000;
	long sixtyOneSec = 61000;
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
	
	@Before
    public void setUp() throws Exception {
		this.mockMvc = mockMvc;
    }


    @Test
    public void ShouldReturnHttpStatusCreatedOnSuccessfulSave() throws Exception {
    	
    	Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min)));
		String inputJson = this.mapToJson(mockTranscation);
    	
        this.mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void ShouldReturnHttpStatusNoContentTransactionISExpired() throws Exception {
    	
    	Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime()))));
		String inputJson = this.mapToJson(mockTranscation);
		
        this.mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isNoContent());
    }
    
    @Test
    public void ShouldReturnHttpStatusUnprocessableTransactionISFuture() throws Exception {
    	
    	Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime()+fiveHrs30Min+10000))));
		String inputJson = this.mapToJson(mockTranscation);
		
        this.mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void ShouldGetTheTransactionStatistics() throws Exception {
    	
    	Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min)));
		String inputJson = this.mapToJson(mockTranscation);
		
		 this.mockMvc.perform(post("/transcation/transcations")
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(inputJson))
	                .andExpect(status().isCreated());

        
        Statistics mockStatistics = new Statistics(BigDecimal.valueOf(4035).setScale(2, RoundingMode.HALF_UP),
        		BigDecimal.valueOf(1008.75).setScale(2, RoundingMode.HALF_UP),BigDecimal.valueOf(1009).setScale(2, RoundingMode.HALF_UP),
        		BigDecimal.valueOf(1008).setScale(2, RoundingMode.HALF_UP),4);
        String outputJson = this.mapToJson(mockStatistics);
        
        this.mockMvc.perform(get("/transcation/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().string(outputJson));
    }
    

    @Test
    public void deleteAllTranscationsAndReturnNoContentResponse() throws Exception {
    	
    	Transcation mockTranscation1 = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min-thirtySec)));
		String inputJson1 = this.mapToJson(mockTranscation1);
		
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson1))
                .andExpect(status().isCreated());
        
        Transcation mockTranscation2 = new Transcation(BigDecimal.valueOf(1008),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min-twelveSec)));
		String inputJson2 = this.mapToJson(mockTranscation2);
        
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson2))
                .andExpect(status().isCreated());

        Thread.sleep(1000);
        
        this.mockMvc.perform(delete("/transcation/transcations"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void ShouldOnlyConsiderTheTransactionsHappenedInLast60Sec() throws Exception {
    	
    	Transcation mockTranscation1 = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min-thirtySec)));
		String inputJson1 = this.mapToJson(mockTranscation1);
		
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson1))
                .andExpect(status().isCreated());
        
        Transcation mockTranscation2 = new Transcation(BigDecimal.valueOf(1008),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min-twelveSec)));
		String inputJson2 = this.mapToJson(mockTranscation2);
        
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson2))
                .andExpect(status().isCreated());

        Transcation mockTranscation3 = new Transcation(BigDecimal.valueOf(1007),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min-sixtyOneSec)));
		String inputJson3 = this.mapToJson(mockTranscation3);
		
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson3))
                .andExpect(status().isNoContent());

        Thread.sleep(1000);

        Statistics mockStatistics = new Statistics(BigDecimal.valueOf(2017).setScale(2, RoundingMode.HALF_UP),
        		BigDecimal.valueOf(1008.5).setScale(2, RoundingMode.HALF_UP),
        		BigDecimal.valueOf(1009).setScale(2, RoundingMode.HALF_UP),
        		BigDecimal.valueOf(1008).setScale(2, RoundingMode.HALF_UP),2);
        String outputJson = this.mapToJson(mockStatistics);
        
        this.mockMvc.perform(get("/transcation/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().string(outputJson));
    } 
    
    private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}