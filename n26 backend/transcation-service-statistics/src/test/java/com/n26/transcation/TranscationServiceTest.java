package com.n26.transcation;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.rules.ExpectedException.none;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.exception.TranscationServiceUnprocessable;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;
import com.n26.transcation.service.TranscationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TranscationServiceTest {

    @Rule
    public final ExpectedException exceptionThrown = none();
    
    @MockBean
    private TranscationService transcationService;

    long fiveHrs30Min = 19800000;
    private Map<Date,List<Transcation>> map = new HashMap<>();
    
    private List<Transcation> list = new ArrayList<>();
    
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
	
    @Before
    public void setUp() throws Exception {
        transcationService = new TranscationServiceImpl();
        this.map = map;
    }

    @Test
    public void testSaveTheTransaction() throws TranscationOverSixtySeconds,Exception {
        Transcation validTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min)));

        transcationService.addTranscation(validTranscation);
        list.add(validTranscation);
        map.put(validTranscation.getTimestamp(), list);
        assertThat(map.get(validTranscation.getTimestamp()).get(0)).isEqualTo(validTranscation);
    }

    @Test
    public void shouldConsiderLast60SecTransactionsInStatistics() throws TranscationOverSixtySeconds,Exception {
        
    	List<Transcation> list = new ArrayList<>();
    	BigDecimal amount = BigDecimal.valueOf(1009);
        Date transcationTime = sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min));
        Date staleTranscationTime = sdf.parse(sdf.format((new Date().getTime())+fiveHrs30Min-61000));
        Transcation validTranscation = new Transcation(amount, transcationTime);
        list.add(validTranscation);
        Transcation staleTranscation = new Transcation(amount, staleTranscationTime);
        list.add(staleTranscation);
        map.put(transcationTime, list);
        map.put(staleTranscationTime, list);

        Statistics transcationStatistics = new Statistics(BigDecimal.valueOf(1009).setScale(2, RoundingMode.HALF_UP), BigDecimal.valueOf(1009).setScale(2, RoundingMode.HALF_UP), BigDecimal.valueOf(1009).setScale(2, RoundingMode.HALF_UP), BigDecimal.valueOf(1009).setScale(2, RoundingMode.HALF_UP),1);

        Statistics currentStatistics = transcationService.getStatistics();
        assertThat(currentStatistics).isEqualTo(transcationStatistics);
    }

    @Test
    public void shouldThrowExceptionIfTransactionIsExpired() throws TranscationOverSixtySeconds,Exception {
        
    	Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime()))));
        exceptionThrown.expect(TranscationOverSixtySeconds.class);

       transcationService.addTranscation(mockTranscation);
    }

    @Test
    public void shouldThrowUnprocessableExceptionIfTransactionIsFuture() throws TranscationServiceUnprocessable,Exception {
        
    	Transcation mockTranscation = new Transcation(BigDecimal.valueOf(1009),sdf.parse(sdf.format((new Date().getTime()+fiveHrs30Min+10000))));
        exceptionThrown.expect(TranscationServiceUnprocessable.class);

       transcationService.addTranscation(mockTranscation);
    }
    
    @Test
    public void testDeleteTheTransaction() throws ParseException  {
        transcationService.deleteAllTransactions();
        assertThat(map.isEmpty()).isEqualTo(true);
    }

}
