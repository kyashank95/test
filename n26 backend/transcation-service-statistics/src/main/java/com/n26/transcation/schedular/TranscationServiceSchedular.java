package com.n26.transcation.schedular;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;

@Component
public class TranscationServiceSchedular {
	
	@Autowired
	private TranscationService transcationService;
	
	long fiveHrs30Min = 19800000;
	
	@Scheduled(fixedRate = 60000)
	private void deleteUnnessaryTranscation() {
	Map<Date, List<Transcation>> mapTranscation = transcationService.getAllTranscation();
		mapTranscation.entrySet().stream().forEach(obj -> {
			Date d = obj.getKey();
			long eachTimeStamp = d.getTime()-fiveHrs30Min;
			long timeCheck = ((System.currentTimeMillis()) - eachTimeStamp)/1000;
			if(timeCheck >= 60) {
				mapTranscation.remove(obj.getKey());
			}
		});
	}

}
