package com.n26.transcation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * Transcation Service Spring Boot Application file
 *
 */
@SpringBootApplication
@EnableScheduling
public class TranscationServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(TranscationServiceApplication.class, args);
	}
	
}
