package com.n26.transcation.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.exception.TranscationServiceInvalidJson;
import com.n26.transcation.exception.TranscationServiceUnprocessable;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;


/***
 * TranscationService interface
 *
 */
@Service
public interface TranscationService {
	
	
	/**
	 * Get All the transcations
	 * @return
	 */
	public Map<Date,List<Transcation>> getAllTranscation();
	
	

	/**
	 * Add a Transcation
	 * @param transcation
	 * @return
	 */
	public Map<Date,List<Transcation>> addTranscation(Transcation transcation)throws TranscationOverSixtySeconds,TranscationServiceUnprocessable;
	
	
	/**
	 * get statistics for last 60 seconds
	 * @return
	 */
	public Statistics getStatistics();



	public void deleteAllTransactions();
	
}
