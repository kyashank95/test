package com.n26.transcation.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.exception.TranscationServiceInvalidJson;
import com.n26.transcation.exception.TranscationServiceUnprocessable;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;


/***
 * Transcation Service Rest Controller
 *
 */
@RestController
@RequestMapping("/transcation")
public class TranscationServiceController {
	
	
	@Autowired
	private TranscationService transcationService;
	
	
	 @Autowired
	    public TranscationServiceController(TranscationService transcationService) {
	        this.transcationService = transcationService;
	    }
	
	
	/**
	 * Adding Transcation
	 * @param transcation
	 * @return
	 */
	@PostMapping("/transcations")
	public ResponseEntity<?> addTranscation(@Valid @RequestBody Transcation transcation) {
		try {
		transcationService.addTranscation(transcation);
		return new ResponseEntity<>(HttpStatus.CREATED);
		}catch(TranscationOverSixtySeconds tot) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch(TranscationServiceUnprocessable unprocess) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	/**
	 * Get All The Statistics
	 * @return
	 */
	@GetMapping("/statistics")
	public ResponseEntity<?> getStatistics() {
		Statistics statistics = transcationService.getStatistics();
		 return new ResponseEntity<>(statistics,HttpStatus.OK);
	}	
	
	@DeleteMapping("/transcations")
    public ResponseEntity<?> deleteTransactions() {
        this.transcationService.deleteAllTransactions();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
