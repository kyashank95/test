package com.n26.transcation.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;




public class Transcation implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@NotNull
	private BigDecimal amount;
	
	@NotNull
	private Date timestamp;
	
	
	public Transcation() {
		// TODO Auto-generated constructor stub
	}


	public Transcation(BigDecimal amount, Date timestamp) {
		super();
		this.amount = amount;
		this.timestamp = timestamp;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Date getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}
