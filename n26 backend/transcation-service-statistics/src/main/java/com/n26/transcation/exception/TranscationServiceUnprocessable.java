package com.n26.transcation.exception;

import com.n26.transcation.model.Transcation;

/**
 * Transcation Service Exception Handler
 * If the Epoch time is greater than 60 seconds
 * @return
 */
public class TranscationServiceUnprocessable extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public TranscationServiceUnprocessable() {
		// TODO Auto-generated constructor stub
	}
	
	public TranscationServiceUnprocessable(Transcation transcation) {
		super();
	}
}
