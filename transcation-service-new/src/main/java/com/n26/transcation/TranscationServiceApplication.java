package com.n26.transcation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * update the comments
 *
 */
@SpringBootApplication
public class TranscationServiceApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(TranscationServiceApplication.class, args);
	}
	
}
