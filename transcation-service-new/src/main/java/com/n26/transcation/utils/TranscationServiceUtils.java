package com.n26.transcation.utils;

import org.springframework.stereotype.Component;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Transcation;

@Component
public class TranscationServiceUtils {
	
	public void calculateTranscationTime(Transcation transcation) throws TranscationOverSixtySeconds {
		long currTime = currentTimeStamp();
		long enteredTimeStamp = transcation.getTimestamp();
		long gapInTimeStamp = currTime - enteredTimeStamp;
		long gapInTimeStampInMilliSec = gapInTimeStamp/1000;
		
		if(gapInTimeStampInMilliSec > 60) {
			throw new TranscationOverSixtySeconds();
		}
	}
	
	public long currentTimeStamp() {
		long currTimeStamp = System.currentTimeMillis();
		return currTimeStamp;
	}


}
