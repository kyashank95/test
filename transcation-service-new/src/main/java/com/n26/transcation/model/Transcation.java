package com.n26.transcation.model;

import java.io.Serializable;


public class Transcation implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private double amount;
	
	private long timestamp;
	
	
	public Transcation() {
		// TODO Auto-generated constructor stub
	}


	public Transcation(double amount, long timestamp) {
		super();
		this.amount = amount;
		this.timestamp = timestamp;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public long getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	
	

}
