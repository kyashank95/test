package com.n26.transcation.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.utils.TranscationServiceUtils;


/***
 * update the comments
 *
 */
@Service
public class TranscationServiceImpl implements TranscationService {

	
	@Autowired
	private TranscationServiceUtils transcationServiceUtils;
	
	private double sum;
	private double avg;
	private double max;
	private double min;
	private long count;
	
	private List<Transcation> listTranscation = new ArrayList<>();
	private Map<Long,List<Transcation>> mapTranscation = new HashMap<>();
	
	@Override
	public Map<Long,List<Transcation>> getAllTranscation() {
		return mapTranscation;
	}


	@Override
	public Map<Long,List<Transcation>> addTranscation(Transcation transcation) throws TranscationOverSixtySeconds {
		transcationServiceUtils.calculateTranscationTime(transcation);
		if(mapTranscation.containsKey(transcation.getTimestamp())) {
			listTranscation.add(transcation);
			mapTranscation.put(transcation.getTimestamp(), listTranscation);			
		}
		else {
			listTranscation = new ArrayList<>();
			listTranscation.add(transcation);
			mapTranscation.put(transcation.getTimestamp(), listTranscation);
		}
		return mapTranscation;
	}


	@Override
	public Statistics getStatistics() {
		long currentTimeStamp = transcationServiceUtils.currentTimeStamp();
		List<Transcation> transcationList = new ArrayList<>();
		mapTranscation.entrySet().stream().forEach(transcationObject ->{ 
			long eachTimeStamp = transcationObject.getKey();
			long timeCheck = (currentTimeStamp - eachTimeStamp)/1000;
			if(timeCheck <= 60) {
				transcationObject.getValue().stream().forEach(correctTranscations -> {
					transcationList.add(correctTranscations);
				});
			}
			});
		return calculateStatistics(transcationList);
	}


	private Statistics calculateStatistics(List<Transcation> transcationList) {
	
		Statistics statistics = new Statistics();
		reInitilize();
		if(!transcationList.isEmpty()) {
			min = Double.valueOf(transcationList.get(0).getAmount());
		transcationList.stream().forEach(trans -> {
			//CompletableFuture<Double> futureSum = new CompletableFuture<>();
			//ExecutorService threadPool2 = Executors.newCachedThreadPool();
			//threadPool2.submit(() -> {
		    sum = sum + trans.getAmount();
		    //futureSum.complete(sum);
			//});
			if(max <= trans.getAmount()) {
				//CompletableFuture<Double> futureMax = new CompletableFuture<>();
				//ExecutorService threadPool3 = Executors.newCachedThreadPool();
				//threadPool3.submit(() -> {
				max = trans.getAmount();
				//futureMax.complete(max);
				//});
			}
			if(min >= trans.getAmount()) {
				//CompletableFuture<Double> futureMin = new CompletableFuture<>();
				//ExecutorService threadPool4 = Executors.newCachedThreadPool();
				//threadPool4.submit(() -> {
				min = trans.getAmount();
				//futureMin.complete(min);
				//});
			}
			});
		//CompletableFuture<Long> futureCount = new CompletableFuture<>();
		//ExecutorService threadPool5 = Executors.newCachedThreadPool();
		//threadPool5.submit(() -> {
		count = transcationList.size();
		//futureCount.complete(count);
		//});
		//CompletableFuture<Double> futureAvg = new CompletableFuture<>();
		//ExecutorService threadPool1 = Executors.newCachedThreadPool();
		//threadPool1.submit(() -> {
		avg = (sum /count);
		//futureAvg.complete(avg);
		//});
		}
		
		statistics.setSum(sum);
		statistics.setAvg(avg);
		statistics.setMax(max);
		statistics.setMin(min);
		statistics.setCount(count);
		
		return statistics;
	}

	public void reInitilize()
	{
		 this.sum = 0.0;
		 this.avg = 0.0;
		 this.max = 0.0;
		 this.min = 0.0;
		 this.count = 0L;
	}
}
