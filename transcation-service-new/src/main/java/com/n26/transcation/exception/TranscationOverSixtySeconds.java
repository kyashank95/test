package com.n26.transcation.exception;

import com.n26.transcation.model.Transcation;

/**
 * If the Epoch time is greater than 60 seconds
 * @return
 */
public class TranscationOverSixtySeconds extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public TranscationOverSixtySeconds() {
		// TODO Auto-generated constructor stub
	}
	
	public TranscationOverSixtySeconds(Transcation transcation) {
		super();
	}
}
