package com.n26.transcation;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;


@RunWith(SpringRunner.class)
@SpringBootTest(classes =TranscationServiceApplication.class)
@AutoConfigureMockMvc
public class IntregationTests {
    
	@Autowired
    private MockMvc mockMvc;
   
	
	@Before
    public void setUp() throws Exception {
		this.mockMvc = mockMvc;
    }


    @Test
    public void ShouldReturnHttpStatusCreatedOnSuccessfulSave() throws Exception {
    	
    	Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(System.currentTimeMillis());
		
		String inputJson = this.mapToJson(mockTranscation);
    	
        this.mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isCreated());
    }

    @Test
    public void ShouldReturnHttpStatusNoContentTransactionISExpired() throws Exception {
    	
    	Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(1579205840000L);
		
		String inputJson = this.mapToJson(mockTranscation);
		
        this.mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(status().isNoContent());
    }

    @Test
    public void ShouldGetTheTransactionStatistics() throws Exception {
    	
    	Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(System.currentTimeMillis());
		
		String inputJson = this.mapToJson(mockTranscation);
		
		 this.mockMvc.perform(post("/transcation/transcations")
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(inputJson))
	                .andExpect(status().isCreated());

        
        Statistics mockStatistics = new Statistics();
        mockStatistics.setSum(1009);
        mockStatistics.setMax(1009);
        mockStatistics.setMin(1009);
        mockStatistics.setAvg(1009);
        mockStatistics.setCount(1);
        
        String outputJson = this.mapToJson(mockStatistics);
        
        this.mockMvc.perform(get("/transcation/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().string(outputJson));
    }

    @Test
    public void ShouldOnlyConsiderTheTransactionsHappenedInLast60Sec() throws Exception {
    	

    	Transcation mockTranscation1 = new Transcation();
		mockTranscation1.setAmount(1009);
		mockTranscation1.setTimestamp(Instant.now().minusSeconds(30).toEpochMilli());
		
		String inputJson1 = this.mapToJson(mockTranscation1);
		
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson1))
                .andExpect(status().isCreated());
        
        Transcation mockTranscation2 = new Transcation();
		mockTranscation2.setAmount(1008);
		mockTranscation2.setTimestamp(Instant.now().minusSeconds(12).toEpochMilli());
		
		String inputJson2 = this.mapToJson(mockTranscation2);
        
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson2))
                .andExpect(status().isCreated());

        Transcation mockTranscation3 = new Transcation();
		mockTranscation3.setAmount(1007);
		mockTranscation3.setTimestamp(Instant.now().minusSeconds(61).toEpochMilli());
		
		String inputJson3 = this.mapToJson(mockTranscation3);
		
        mockMvc.perform(post("/transcation/transcations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson3))
                .andExpect(status().isNoContent());

        Thread.sleep(1000);

        Statistics mockStatistics = new Statistics();
        mockStatistics.setSum(2017);
        mockStatistics.setMax(1009);
        mockStatistics.setMin(1008);
        mockStatistics.setAvg(1008.5);
        mockStatistics.setCount(2);
        
        String outputJson = this.mapToJson(mockStatistics);
        
        this.mockMvc.perform(get("/transcation/statistics"))
                .andExpect(status().isOk())
                .andExpect(content().string(outputJson));
    }
    
    private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}