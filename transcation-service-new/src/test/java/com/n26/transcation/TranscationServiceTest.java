package com.n26.transcation;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.rules.ExpectedException.none;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;
import com.n26.transcation.service.TranscationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TranscationServiceTest {

    @Rule
    public final ExpectedException exceptionThrown = none();
    private TranscationService transcationService;

    private Map<Long,List<Transcation>> map = new HashMap<>(); 
    
    @Before
    public void setUp() throws Exception {
        transcationService = new TranscationServiceImpl();
        this.map = map;
    }

    @Test
    public void testSaveTheTransaction() throws TranscationOverSixtySeconds {
        double amount = 1009;
        long transactionTime = System.currentTimeMillis();
        Transcation validTranscation = new Transcation(amount, transactionTime);

        transcationService.addTranscation(validTranscation);
        assertThat(map.get(transactionTime)).isEqualTo(validTranscation);
    }

    @Test
    public void shouldConsiderLast60SecTransactionsInStatistics() throws TranscationOverSixtySeconds {
        
    	List<Transcation> list = new ArrayList<>();
    	double amount = 1009;
        long transcationTime = System.currentTimeMillis();
        long staleTranscationTime = Instant.now(Clock.systemUTC()).minusSeconds(61).toEpochMilli();
        Transcation validTranscation = new Transcation(amount, transcationTime);
        list.add(validTranscation);
        Transcation staleTranscation = new Transcation(amount, staleTranscationTime);
        list.add(staleTranscation);
        map.put(transcationTime, list);
        map.put(transcationTime, list);

        Statistics transcationStatistics = new Statistics(1009, 1009, 1009, 1009,1);

        Statistics currentStatistics = transcationService.getStatistics();
        assertThat(currentStatistics).isEqualTo(transcationStatistics);
    }

    @Test
    public void shouldThrowExceptionIfTransactionIsExpired() throws TranscationOverSixtySeconds {
        
    	Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(1579205840000L);
		
        exceptionThrown.expect(TranscationOverSixtySeconds.class);

        transcationService.addTranscation(mockTranscation);
    }

}
