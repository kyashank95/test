package com.n26.transcation;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transcation.model.Transcation;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TranscationServiceApplication.class)
@AutoConfigureMockMvc
public class TranscationServiceControllerApplicationTests {
	
	
	@Autowired
	private MockMvc mockMvc;
		
	@Test
	public void mockAddTranscationCheckNoContent() throws Exception {
	
		Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(1579205840000L);
		
		String inputJson = this.mapToJson(mockTranscation);
		
	RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/transcation/transcations")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(inputJson);
		
		 mockMvc.perform(requestBuilder).andExpect(status().isNoContent());
	}
	
	@Test
	public void mockAddTranscationCheckCreated() throws Exception {
	
		Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(System.currentTimeMillis());
		
		String inputJson = this.mapToJson(mockTranscation);
		
	RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/transcation/transcations")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(inputJson);
		
		 mockMvc.perform(requestBuilder).andExpect(status().isCreated());
	}
	
	@Test
	public void mockGetStatistics() throws Exception {
	
		
	RequestBuilder requestBuilder = MockMvcRequestBuilders
				.get("/transcation/statistics")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);
		
		 mockMvc.perform(requestBuilder).andExpect(status().isOk());
	}
	
	
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
}
