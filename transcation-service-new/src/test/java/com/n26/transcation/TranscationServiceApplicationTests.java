package com.n26.transcation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.transcation.exception.TranscationOverSixtySeconds;
import com.n26.transcation.model.Statistics;
import com.n26.transcation.model.Transcation;
import com.n26.transcation.service.TranscationService;
import com.n26.transcation.utils.TranscationServiceUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TranscationServiceApplication.class)
@AutoConfigureMockMvc
public class TranscationServiceApplicationTests {
	
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private TranscationService transcationService;
	
	private double sum;
	private double avg;
	private double max;
	private double min;
	private long count;
	private List<Transcation> mocklistTranscation = new ArrayList<>();
	private Map<Long,List<Transcation>> mockmapTranscation = new HashMap<>();
	
	@Test
	public void mockaddTranscation() throws TranscationOverSixtySeconds {
		Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(System.currentTimeMillis());
		
		if(mockmapTranscation.containsKey(mockTranscation.getTimestamp())) {
			mocklistTranscation.add(mockTranscation);
			mockmapTranscation.put(mockTranscation.getTimestamp(), mocklistTranscation);			
		}
		else {
			mocklistTranscation = new ArrayList<>();
			mocklistTranscation.add(mockTranscation);
			mockmapTranscation.put(mockTranscation.getTimestamp(), mocklistTranscation);
		}
		Assert.assertEquals(mockmapTranscation,transcationService.addTranscation(mockTranscation));
		
	}
	
	
	@Test(expected = TranscationOverSixtySeconds.class)
	public void mockCalculateTimeOverSixty() throws TranscationOverSixtySeconds {
		Transcation mockTranscation = new Transcation();
		mockTranscation.setAmount(1009);
		mockTranscation.setTimestamp(1579205840000L);
		transcationService.addTranscation(mockTranscation);			
	}
	
	@Test
	public void mockCalculateStaistics() throws JsonProcessingException  {
		
		for(int i=1006;i<=1010;i++) {
			Transcation mockTranscation = new Transcation();
			mockTranscation.setAmount(i);
			mockTranscation.setTimestamp(System.currentTimeMillis());
			mocklistTranscation.add(mockTranscation);
		}
		
		Statistics mockstatistics = new Statistics();
		reInitilize();
		if(!mocklistTranscation.isEmpty()) {
			min = Double.valueOf(mocklistTranscation.get(0).getAmount());
			mocklistTranscation.stream().forEach(trans -> {
		    sum = sum + trans.getAmount();
			if(max <= trans.getAmount()) {
				max = trans.getAmount();
			}
			if(min >= trans.getAmount()) {
				min = trans.getAmount();
			}
			});
		count = mocklistTranscation.size();
		avg = (sum /count);
		}
		
		mockstatistics.setSum(sum);
		mockstatistics.setAvg(avg);
		mockstatistics.setMax(max);
		mockstatistics.setMin(min);
		mockstatistics.setCount(count);
		
		String getJson = this.mapToJson(mockstatistics);
		System.out.println("getJson---------->"+getJson);
		String expected = "{\"sum\":5040.0,\"avg\":1008.0,\"max\":1010.0,\"min\":1006.0,\"count\":5.0}" ;
		assertEquals(expected, getJson);
	}
		
	
	
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
	
	public void reInitilize()
	{
		 this.sum = 0.0;
		 this.avg = 0.0;
		 this.max = 0.0;
		 this.min = 0.0;
		 this.count = 0L;
	}
	
}
